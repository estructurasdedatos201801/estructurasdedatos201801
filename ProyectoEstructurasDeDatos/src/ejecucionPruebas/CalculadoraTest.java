/**
 * 
 */
package ejecucionPruebas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import capitulo0.ejemplo.Calculadora;

public class CalculadoraTest {

	Calculadora calculadoraDePrueba = new Calculadora();

	@Test
	public void testSumar() {
		int resultadoEsperado = 4;
		int resultadoObtenido = calculadoraDePrueba.sumar(2, 2);
		assertEquals(resultadoEsperado, resultadoObtenido);
	}

	@Test
	public void testRestar() {
		int resultadoEsperado = 100;
		int resultadoObtenido = calculadoraDePrueba.restar(120, 20);
		assertEquals(resultadoEsperado, resultadoObtenido);
	}

	@Test
	public void testMultiplicar() {
		int resultadoEsperado = 200;
		int resultadoObtenido = calculadoraDePrueba.multiplicar(100, 2);
		assertEquals(resultadoEsperado, resultadoObtenido);
	}

	@Test
	public void testDividir() {
		//Probando division normal
		float resultadoEsperado = 100;
		float resultadoObtenido = calculadoraDePrueba.dividir(200, 2);
		assertEquals(resultadoEsperado, resultadoObtenido, 0);
		//Probando division por cero
		try {
			float resultadoObtenidoDivisionPorCero = calculadoraDePrueba.dividir(200, 0);
			fail("La divisi�n por cero deber�a fallar" + resultadoObtenidoDivisionPorCero);
		} catch (IllegalArgumentException e) {
			Assert.assertTrue(true);
		}

	}
	
	@Test
	public void reemplazar() {
		
	}
	

}

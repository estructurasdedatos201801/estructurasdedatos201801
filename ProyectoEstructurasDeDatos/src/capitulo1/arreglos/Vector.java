package capitulo1.arreglos;

public class Vector {

	// Definir atributos

	public int vector[];

	//Constructor, es la funcion que se llama al crear un nuevo vector
	// Un nuevo vector se crea as� desde cualquier lugar del proyecto: 
	// Vector miVector = new Vector(10); <-- ese 10 es el que recibe el contructor 
	public Vector(int tam) {
		this.vector = new int[tam];
	}

	// Definir m�todos
	public void agregarElemento() {
		System.out.println("Algoritmo para insertar");
	}

	public void eliminarElemento() {
		System.out.println("Algoritmo para eliminar");
	}

	public void listarElementos() {
		System.out.println("Algoritmo para listar");
	}

	public void buscarElemento() {
		System.out.println("Algoritmo para buscar");
	}
}

package capitulo0.ejemplo;

public class Calculadora {

	public int sumar(int a, int b) {
		return a + b;
	}

	public int restar(int a, int b) {
		return a - b;
	}

	public int multiplicar(int a, int b) {
		return a * b;
	}

	public float dividir(int a, int b) {
		if(b==0) {
			//Tip Avanzado: asi se lanza algo que se llama excepciones, son errores esperados y personalizados, muy utiles 
			//para saber que hay algo mal que uno espera que suceda, y devolver errores personalizados
			throw new IllegalArgumentException("Divisi�n por cero no admitiva, el divisor no puede ser cero");
		}
		return a / b;
	}
}
